SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `inventories` (`user_id`, `name`, `created`) VALUES
(0, 'eain', '2012-11-05 11:47:45'),
(1, 'hello world', '2012-11-05 11:49:10');

INSERT INTO `users` (`user_id`, `name`, `password`, `created`) VALUES
(1, 'rain', '123456', '2012-11-06 00:00:00'),
(2, 'test', '324324r', '2012-11-30 00:00:00');
SET FOREIGN_KEY_CHECKS=1;

