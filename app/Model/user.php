<?php
class User extends AppModel
{
    public $primaryKey = 'user_id';

    public $hasMany = array(
        'Inventory' => array(
            'className'     => 'Inventory',
            'foreignKey'    => 'user_id',
            'order'         => 'Inventory.created DESC',
            'limit'         => '5',
            'dependent'     => true
        )
    );
}