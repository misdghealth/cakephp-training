<?php
class Inventory extends AppModel {

    public $primaryKey = 'user_id';

    public $tableName  = 'inventories';

    public $belongsTo  = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'user_id'
        )
    );
}